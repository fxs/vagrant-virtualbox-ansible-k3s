# encoding: utf-8
# copyright: 2020, The Authors

title 'Devsec checks'

include_controls 'devsec-linux' do
    # IPv4 Forwarding: Docker
    skip_control 'sysctl-01'
end

include_controls 'devsec-ssh'
