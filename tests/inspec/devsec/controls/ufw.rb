# encoding: utf-8
# copyright: 2020, The Authors

title 'UFW checks'

control 'ufw-01' do
  title 'ufw enabled'
  describe command('ufw status') do
    its('stdout') { should include 'Status: active' }
  end
end

control 'ufw-02' do
  title 'default policy deny incoming'
  describe iptables do
    it { should have_rule('-P INPUT DROP') }
  end
  describe command('ufw status verbose') do
    its('stdout') { should include 'Default: deny (incoming), allow (outgoing), deny (routed)' }
  end
end

control 'ufw-03' do
  title 'ssh should be authorized for bastion'
  describe command('ufw status') do
    its('stdout') { should include '22/tcp' }
    its('stdout') { should include input('ip_bastion') }
  end
  describe file('/etc/ufw/user.rules') do
    its('content') { should include '-A ufw-user-input -p tcp --dport 22 -s ' + input('ip_bastion') + ' -j ACCEPT' }
  end
end
