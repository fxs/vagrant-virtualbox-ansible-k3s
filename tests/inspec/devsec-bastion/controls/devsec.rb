# encoding: utf-8
# copyright: 2020, The Authors

title 'Devsec checks'

include_controls 'devsec-linux'

include_controls 'devsec-ssh' do
  skip_control 'sshd-39'
end
