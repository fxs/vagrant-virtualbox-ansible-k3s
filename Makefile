
VAGRANT      = vagrant
ANSIBLE-LINT = ansible-lint
SSH          = ssh
GIT          = git
DOCKER       = docker
ANSIBLE      = ansible

IMAGE_INSPEC = chef/inspec

LOGIN_USER   = vagrant
INVENTORY_FILE = provisioning/local
ROOT_PLAYBOOK = provisioning/site.yml

## Vagrant management
## ------------------
v-init: ## Vagrant up
v-init:
	$(eval start := $(shell date))
	$(VAGRANT) up
	@echo "---"
	@echo "Start: ${start} - End: $$(date)"

vb-start: ## Start all Virtualbox instances
vb-start: list-vm-id
	@while read -r l; do \
		VBoxManage startvm "$$l" --type headless; \
	done <.vm/id

vb-stop: ## Stop all Virtualbox instances
vb-stop: list-vm-id
	@while read -r l; do \
		VBoxManage controlvm "$$l" acpipowerbutton; \
	done <.vm/id

v-destroy: ## Vagrant destroy
v-destroy:
	$(VAGRANT) destroy -f

v-box-list: ## Vagrant box list
v-box-list:
	$(VAGRANT) box list

v-box-update: ## Vagrant box update
v-box-update:
	$(VAGRANT) box update

v-box-remove: ## Vagrant box remove. Argument: version=20181129.0.0
v-box-remove:
	test -n "${version}"  # Failed if host not set
	$(VAGRANT) box remove ubuntu/bionic64 --box-version "${version}"

.PHONY: v-up v-provision vb-start vb-stop v-destroy v-box-list v-box-update v-box-remove

## Ansible management
## ------------------
provision: ## Provisioning with Ansible. Argument [group=bastions]
provision:
	$(eval start := $(shell date))
	@if [ -z ${group} ]; then \
		ansible-playbook -i ${INVENTORY_FILE} ${ROOT_PLAYBOOK}; \
	else \
		ansible-playbook --limit ${group} -i ${INVENTORY_FILE} ${ROOT_PLAYBOOK}; \
	fi
	@echo "---"
	@echo "Start: ${start} - End: $$(date)"

ansible-galaxy-install: ## Install requirements from Galaxy.
ansible-galaxy-install:
	ansible-galaxy install --force -r provisioning/requirements.yml

.PHONY: ansible-galaxy-install

## Test
## ----
check: ## Check syntax
check:
	$(ANSIBLE-LINT) ${ROOT_PLAYBOOK}

inspec-dev-sec-bastion: ## dev-sec.io tests with Inspec for bastion.
inspec-dev-sec-bastion:
	$(eval ip_bastion := $(shell cat ${INVENTORY_FILE} | grep bastion | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(DOCKER) run -it --rm -v ${PWD}:/share ${IMAGE_INSPEC} exec tests/inspec/devsec-bastion --chef-license accept-no-persist --no-distinct-exit --sudo --no-create-lockfile -t ssh://${LOGIN_USER}@${ip_bastion} -i .vagrant/machines/bastion/virtualbox/private_key

inspec-dev-sec: ## dev-sec.io tests with Inspec for host. Argument: host=master-1|node-1|...
inspec-dev-sec:
	test -n "${host}"  # Failed if host not set
	$(eval ip_host := $(shell cat ${INVENTORY_FILE} | grep ${host} | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(eval ip_bastion := $(shell cat ${INVENTORY_FILE} | grep bastion | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	$(DOCKER) run -it --rm -v ${PWD}:/share ${IMAGE_INSPEC} exec tests/inspec/devsec --input ip_bastion=${ip_bastion} --chef-license accept-no-persist --no-distinct-exit --sudo --no-create-lockfile --proxy-command="ssh ${LOGIN_USER}@${ip_bastion} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i .vagrant/machines/bastion/virtualbox/private_key -W %h:%p" -t ssh://${LOGIN_USER}@${ip_host} -i .vagrant/machines/${host}/virtualbox/private_key

get-inspec-dev-sec-profile: ## Get Inspec dev-sec.io profiles.
get-inspec-dev-sec-profile:
	@mkdir -p .inspec
	@rm -rf .inspec/linux-baseline
	${GIT} clone --single-branch --branch master https://github.com/dev-sec/linux-baseline.git .inspec/linux-baseline
	@rm -rf .inspec/linux-baseline/.git
	@rm -rf .inspec/ssh-baseline
	${GIT} clone --single-branch --branch master https://github.com/dev-sec/ssh-baseline.git .inspec/ssh-baseline
	@rm -rf .inspec/ssh-baseline/.git

.PHONY: check inspec-dev-sec-bastion inspec-dev-sec get-inspec-dev-sec-profile

## Admin
## -----
ssh: ## SSH connexion. Argument: host=bastion|node-1|...
ssh:
	test -n "${host}"  # Failed if host not set
	$(eval ip_host := $(shell cat ${INVENTORY_FILE} | grep ${host} | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}' | sort -u | head -n 1))
	@if [ "${host}" = "bastion" ]; then \
		$(SSH) -F ssh_config ${host}; \
	else \
		$(SSH) -F ssh_config -i .vagrant/machines/${host}/virtualbox/private_key ${ip_host}; \
	fi

update: ## Update vagrant box, dev-sec profile, docker images
update:
	@$(MAKE) v-box-update
	@$(MAKE) get-inspec-dev-sec-profile
	@$(MAKE) ansible-galaxy-install
	$(DOCKER) pull ${IMAGE_INSPEC}

.PHONY: ssh update

# Private functions
# #################
list-vm-id:
	@rm -rf .vm
	@mkdir -p .vm
	@touch .vm/id
	@cd .vagrant/machines && find . -maxdepth 1 -type d | sed 's/\.//' | sed 's/\///' | sed '/^$$/d' > ../../.vm/vm
	@while read -r l; do \
		if [ -f ".vagrant/machines/$$l/virtualbox/id" ]; then awk '{print $0}' ".vagrant/machines/$$l/virtualbox/id" >> .vm/id; fi \
	done <.vm/vm

PHONY: list-vm-id

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
