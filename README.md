# vagrant-virtualbox-ansible-k3s

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)

A ready to use environment to play with [k3s](https://k3s.io/).

## Getting Started

### Prerequisities

In order to deploy this environment:

* [Vagrant](https://www.vagrantup.com/)
* [Virtualbox](https://www.virtualbox.org/)
* [Ansible](https://www.ansible.com/)

Optional but recommended:

* [make](https://www.gnu.org/software/make/)
* [vagrant-vbguest](https://github.com/dotless-de/vagrant-vbguest)
* [docker](https://www.docker.com/)

### Usage

To deploy the environment:

```shell
git clone https://gitlab.com/fxs/vagrant-virtualbox-ansible-k3s.git
cd vagrant-virtualbox-ansible-k3s
make update
make v-up
```

List of available commandes:

```shell
make
```

## Tests

To lint Ansible playbooks:

```shell
make check
```

To run security [Chef Inspec](https://www.chef.io/products/chef-inspec/) tests with [DevSec Hardening Framework](https://dev-sec.io/) OS and SSH Baselines:

```shell
make inspec-dev-sec-bastion
make inspec-dev-sec host=master-1
make inspec-dev-sec host=node-1
...
```

## Find Us

* [Gitlab](https://gitlab.com/fxs/vagrant-virtualbox-ansible-k3s)

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/vagrant-virtualbox-ansible-k3s/blob/master/LICENSE) file for details.
