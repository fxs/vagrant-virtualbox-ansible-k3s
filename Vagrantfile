# -*- mode: ruby -*-
# vi: set ft=ruby :

IMAGE_NAME = "ubuntu/bionic64"
MEM_MIN = 512
CPU_MIN = 1
MEM_DEFAULT = 1024
CPU_DEFAULT = 2
MASTER_COUNT = 1
NODE_COUNT = 2
NETWORK_BASE = "192.168.50"

Vagrant.configure("2") do |config|
    config.vm.define "bastion" do |bastion|

        bastion.vm.box = IMAGE_NAME
        bastion.vm.network "private_network", ip: "#{NETWORK_BASE}.100"
        bastion.vm.hostname = "bastion"

        bastion.vm.provider "virtualbox" do |bv|
            bv.memory = MEM_MIN
            bv.cpus = CPU_MIN
        end

        bastion.vm.provision :ansible do |ansible|

            ansible.compatibility_mode = "2.0"
            ansible.limit = "bastions"
            ansible.playbook = "provisioning/site.yml"
            ansible.inventory_path = "provisioning/local"
        end
    end

    (1..MASTER_COUNT).each do |i|
        config.vm.define "master-#{i}" do |master|

            master.vm.box = IMAGE_NAME
            master.vm.network "private_network", ip: "#{NETWORK_BASE}.#{i + 10}"
            master.vm.hostname = "master-#{i}"

            master.vm.provider "virtualbox" do |mv|
                mv.memory = MEM_DEFAULT
                mv.cpus = CPU_DEFAULT
            end

            if i == MASTER_COUNT
                master.vm.provision :ansible do |ansible|

                    ansible.compatibility_mode = "2.0"
                    ansible.limit = "masters"
                    ansible.playbook = "provisioning/site.yml"
                    ansible.inventory_path = "provisioning/local"
                end
            end
        end
    end

    (1..NODE_COUNT).each do |i|
        config.vm.define "node-#{i}" do |node|

            node.vm.box = IMAGE_NAME
            node.vm.network "private_network", ip: "#{NETWORK_BASE}.#{i + 20}"
            node.vm.hostname = "node-#{i}"

            node.vm.provider "virtualbox" do |nv|
                nv.memory = MEM_DEFAULT
                nv.cpus = CPU_DEFAULT
            end

            if i == NODE_COUNT
                node.vm.provision :ansible do |ansible|

                    ansible.compatibility_mode = "2.0"
                    ansible.limit = "nodes"
                    ansible.playbook = "provisioning/site.yml"
                    ansible.inventory_path = "provisioning/local"
               end
            end
        end
    end
end
